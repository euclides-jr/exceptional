import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/pages/Home';
import Projects from '@/pages/Projects';
import Users from '@/pages/Users';

Vue.use(Router);

export default new Router({
  mode: 'history',
  linkActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects,
    },
    {
      path: '/users/:id?',
      name: 'users',
      component: Users,
    },
  ],
});
