import axios from 'axios';

class Http {
  static create(options) {
    return axios.create(options);
  }
}

export default Http;
