import Http from '@/services/http';

class UserService {
  constructor() {
    this.http = Http.create({ baseURL: '/api' });
  }

  getAllUsers() {
    return this.http.get('/users');
  }

  addNewUser(user) {
    return this.http.post('/users', user);
  }
}

const userService = new UserService();
export default userService;
