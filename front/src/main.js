import 'bulma/bulma.sass';
import Buefy from 'buefy';
import Vue from 'vue';

import App from './App';
import router from './router';
import store from './store';

Vue.use(Buefy);
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
