import * as Mutations from '../types';

export default {
  [Mutations.GET_USERS](state, users) {
    state.users = users;
  },
  [Mutations.INSERT_USER](state, user) {
    state.users = [user, ...state.users];
  },
};
