import UserService from '@/pages/Users/UserService';
import * as Mutations from '../types';

export default {
  loadUsers({ commit }) {
    return UserService.getAllUsers()
      .then(({ data }) => {
        commit(Mutations.GET_USERS, data.content);
      });
  },
  insertUser({ commit }, user) {
    return UserService.addNewUser(user)
      .then(({ data }) => {
        commit(Mutations.INSERT_USER, data);
        return Promise.resolve(true);
      });
  },
};
