import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

import userRoutes from './routes/users-routes';

const app = express();
const server = http.Server(app);
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/exceptional');

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

server.listen(port, () => {
  console.log(`Server is listening at http://localhost:${port}`);
});

app.get('/', (req, res) => {
  res.send('API Version 0.0.1');
});

userRoutes(app);
