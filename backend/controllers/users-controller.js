import mongoose from 'mongoose';
import UserSchema from '../models/user-schema';

const User = mongoose.model('User', UserSchema);

export function getAllUsers(req, res) {
  User.find((err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.json({ content: result });
    }
  });
}

export function addNewUser(req, res) {
  const newUser = new User(req.body);

  newUser.save()
    .then(user => res.json(user))
    .catch(error => res.status(422).send(error));
}
