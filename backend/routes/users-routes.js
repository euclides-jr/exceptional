import { getAllUsers, addNewUser } from '../controllers/users-controller';

export default (app) => {
  app.route('/users')
    .get(getAllUsers)
    .post(addNewUser);
}
