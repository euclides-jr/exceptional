import mongoose from 'mongoose';


export default mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  role: {
    type: String,
    enum: ['Admin', 'Developer', 'Analyst'],
  },
});
